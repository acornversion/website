const routes = [
  {
    path: "/",
    component: () => import("pages/manual.vue"),
    name: "manual"
  },
  {
    path: "/manual02",
    component: () => import("pages/manual02.vue"),
    name: "manual02"
  },
  {
    path: "/tutorial01a",
    component: () => import("pages/tutorial01a.vue"),
    name: "tutorial01a"
  },
  {
    path: "/login",
    component: () => import("pages/login.vue"),
    name: "login"
  },
  {
    path: "/voting",
    component: () => import("pages/voting.vue"),
    name: "voting"
  },

  {
    path: "/vocab",
    component: () => import("pages/vocab.vue"),
    name: "vocab"
  },
  {
    path: "/grammar",
    component: () => import("pages/grammar.vue"),
    name: "grammar"
  },

  {
    path: "/reading",
    component: () => import("pages/reading.vue"),
    name: "reading"
  },
  {
    path: "/phonics",
    component: () => import("pages/phonics.vue"),
    name: "phonics"
  },
  {
    path: "/translation",
    component: () => import("pages/translation.vue"),
    name: "translation"
  },
  {
    path: "/language",
    component: () => import("pages/language.vue"),
    name: "language"
  },
  {
    path: "/user",
    component: () => import("pages/user.vue"),
    name: "user"
  },
  {
    path: "/score",
    component: () => import("pages/score.vue"),
    name: "score"
  },
  {
    path: "/datafile",
    component: () => import("pages/datafile.vue"),
    name: "datafile"
  },
  {
    path: "/game",
    component: () => import("pages/game.vue"),
    name: "game"
  },
  {
    path: "/questions",
    component: () => import("pages/questions.vue"),
    name: "questions"
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;

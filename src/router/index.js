import Vue from "vue";
import VueRouter from "vue-router";

import routes from "./routes";

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCQwjdntZWZAUmlUogaepcMvNaZA_DzQ3Y",
  authDomain: "content-1c8d3.firebaseapp.com",
  databaseURL: "https://content-1c8d3.firebaseio.com",
  projectId: "content-1c8d3",
  storageBucket: "content-1c8d3.appspot.com",
  messagingSenderId: "832583578583",
  appId: "1:832583578583:web:10180415ef0281a3211192"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default function(/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
